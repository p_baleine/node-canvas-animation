# node-canvas-animation

http://evening-basin-5850.herokuapp.com/

[socket.io 1.0のリリース](http://socket.io/blog/introducing-socket-io-1-0/)に感動して作ってみた、サーバサイドで[node-canvas](https://github.com/LearnBoost/node-canvas)でアニメーションしてその画像を`socket.io`経由でクライアントに送信し、クライアントで描画するサンプル。

懐かしいブラウザのアイコンが見えるだけです。
