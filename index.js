var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var swig = require('swig');
var favicon = require('serve-favicon');
var Animation = require('./animation');

server.listen(process.env.PORT || 3000);

var anim = new Animation().start();

app.engine('html', swig.renderFile);

app.set('view engine', 'html');
app.set('views', __dirname);

app.use(favicon(__dirname + '/ie6.png'));

app.get('/', function(req, res) {
  res.render('index');
});

io.sockets.on('connection', function(socket) {
  anim.on('tick', function(buf) {
    socket.emit('tick', { image: true, buffer: buf });
  });
});
