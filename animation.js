var fs = require('fs');
var inherits = require('util').inherits;
var Canvas = require('canvas');
var Image = Canvas.Image;
var EventEmitter = require('events').EventEmitter;

module.exports = Animation;

var WIDTH = 400;
var HEIGHT = 300;

var ie6 = fs.readFileSync(__dirname + '/ie6.png');

function Animation() {
  this.x = 400;
  this.y = 300;
  this.dx = 2;
  this.dy = 4;
  this.r = 10;
  this.timer = null;
  this.canvas = new Canvas(WIDTH, HEIGHT);
  this.ctx = this.canvas.getContext('2d');
}

inherits(Animation, EventEmitter);

Animation.prototype.start = function() {
  this.timer = setInterval(function() {
    this._tick();
    this.emit('tick', this.canvas.toBuffer());
  }.bind(this), 50);
  return this;
};

Animation.prototype._tick = function() {
  this._clear();
  this.ctx.fillStyle = '#faf7f8';
  this._rect();
  this.ctx.fillStyle = '#444444';
  this._circle();

  if (this.x + this.dx > WIDTH || this.x + this.dx < 0) {
    this.dx = - this.dx;
  }
  if (this.y + this.dy > HEIGHT || this.y + this.dy < 0) {
    this.dy = - this.dy;
  }

  this.x += this.dx;
  this.y += this.dy;
};

Animation.prototype.pngStream = function() {
  return this.canvas.pngStream();
};

Animation.prototype._circle = function() {
  var img = new Image();
  img.src = ie6;
  this.ctx.drawImage(img, this.x, this.y, 30, 30);
};

Animation.prototype._rect = function() {
  this.ctx.beginPath();
  this.ctx.rect(0, 0, WIDTH, HEIGHT);
  this.ctx.closePath();
  this.ctx.fill();
};

Animation.prototype._clear = function() {
  this.ctx.clearRect(0, 0, WIDTH, HEIGHT);
};

